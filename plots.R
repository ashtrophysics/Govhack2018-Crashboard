## CHARTS AND GRAPHS ##

directory <- "D:/GovHack2018/"

library(dplyr)
library(ggplot2)
library(gridExtra)

scores <- read.csv(paste0(directory,"app.R/Data/scores.csv"), header = TRUE, stringsAsFactors = FALSE)


# Scores per LGA ----------------------------------------------------------

total_crashes_shires <- read.csv(paste0(directory,"total_crashes_shires.csv"), header = TRUE, stringsAsFactors = FALSE)

total_crashes_shires$category[total_crashes_shires$score < 4] <- "Low"
total_crashes_shires$category[total_crashes_shires$score >= 4 & total_crashes_shires$score < 7] <- "Medium"
total_crashes_shires$category[total_crashes_shires$score >= 7] <- "High"
total_crashes_shires$category[total_crashes_shires$LGA_NAME == "CASEY"] <- "Your score"

total_crashes_shires$category <- factor(total_crashes_shires$category,
                                        levels = c("Low", "Medium", "High", "Your score"))


ggplot(data = total_crashes_shires, aes(x = reorder(LGA_NAME, score), y = score, fill = category)) + 
  geom_bar(stat = 'identity') +
  coord_flip() +
  scale_fill_manual(values=c("#D64D4D", "#E39E54","#66cdaa","#3B3561")) +
  ylab("Score") +  xlab(NULL) + ggtitle("Council Scores Across Victoria") +
  scale_x_discrete(labels = c("CASEY"= expression(bold(CASEY)), parse=TRUE))

# NOTE: weather and light conditions charts were made via dummy data and plotted in excel due to time constraints